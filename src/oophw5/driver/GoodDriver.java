package oophw5.driver;

import oophw5.*;
import oophw5.simulator.*;
import oophw5.highway.*;

public final class GoodDriver extends Driver {
	public GoodDriver() {
		synchronized (Key.key) {
			System.out.println("[Good driver] Good driver is established.");
		}
	}
	
	private boolean safeDistance(Car car, double speed) {
		return ConverterTools.distance(car) > 2 * (speed + 0.1);
	}
	private boolean safeDistance(Car car1, Car car2, double speed) {
		return ConverterTools.distance(car1, car2) > 2 * (speed + 0.1);
	}
	private boolean isDangerous(Car self, Car car) {
		return ConverterTools.distance(self, car) < self.getSpeed();
	}

	protected Decision waitPolicy(Highway highway, Car car) {
		AroundCar cars = new AroundCar(highway, car);
		Car self = cars.self();
		if (Decision.isEntry()) {
			if (Decision.flag[1] == 0 && cars.front() == null)
				return new Decision(Decision.DecisionType.MakeRun, 1);
			else if (Decision.flag[0] == 0 && cars.upfront() == null)
				return new Decision(Decision.DecisionType.MakeRun, 0);
			else if (Decision.flag[2] == 0 && cars.downfront() == null)
				return new Decision(Decision.DecisionType.MakeRun, 2);
			else if (Decision.flag[1] == 0 && safeDistance(cars.front(), self.getSpeed()))
				return new Decision(Decision.DecisionType.MakeRun, 1);
			else if (Decision.flag[0] == 0 && safeDistance(cars.upfront(), self.getSpeed()))
				return new Decision(Decision.DecisionType.MakeRun, 0);
			else if (Decision.flag[2] == 0 && safeDistance(cars.downfront(), self.getSpeed()))
				return new Decision(Decision.DecisionType.MakeRun, 2);
		}
		return new Decision(Decision.DecisionType.DoNothing);
	}
	
	protected Decision runPolicy(Highway highway, Car car) {
		AroundCar cars = new AroundCar(highway, car);
		Car self = cars.self();
		if (cars.self().getPosition() >= highway.getLength())
			return new Decision(Decision.DecisionType.MakeExit);
		
		if (cars.front() == null) {
			if (self.getSpeed() + 0.1 > highway.getSpeedLimit())
				return new Decision(Decision.DecisionType.DoNothing);
			return new Decision(Decision.DecisionType.SpeedUp);
		}
		else if (isUpLane(self) == false && cars.upfront() == null)
			return new Decision(Decision.DecisionType.GoUpside);
		else if (isDownLane(self, highway) == false && cars.downfront() == null)
			return new Decision(Decision.DecisionType.GoDownside);
		
		if (isDangerous(self, cars.front()))
			return new Decision(Decision.DecisionType.MakeStop);
		
		if (safeDistance(self, cars.front(), self.getSpeed())) {
			if (self.getSpeed() + 0.1 > highway.getSpeedLimit())
				return new Decision(Decision.DecisionType.DoNothing);
			return new Decision(Decision.DecisionType.SpeedUp);
		}
		else if (isUpLane(self) == false && safeDistance(self, cars.front(), self.getSpeed()))
			return new Decision(Decision.DecisionType.GoUpside);
		else if (isDownLane(self, highway) == false && safeDistance(self, cars.front(), self.getSpeed()))
			return new Decision(Decision.DecisionType.GoDownside);
		
		return new Decision(Decision.DecisionType.SpeedDown);
	}
	
	protected Decision stopPolicy(Highway highway, Car car) {
		AroundCar cars = new AroundCar(highway, car);
		if (cars.front() == null || ConverterTools.distance(cars.self(), cars.front()) > 0.2) {
			return new Decision(Decision.DecisionType.MakeRun, cars.self().getLane());
		}
		return new Decision(Decision.DecisionType.DoNothing);
	}
	
	protected Decision exitPolicy(Highway highway, Car car) {
		return new Decision(Decision.DecisionType.MakeExit);
	}
}

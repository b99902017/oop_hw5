package oophw5.driver;

public final class Decision {
	public static int[] flag = {0, 0, 0};
	public enum DecisionType {
		DoNothing,
		MakeWait, MakeRun, MakeStop, MakeExit,
		GoUpside, GoDownside,
		SpeedUp, SpeedDown
	}
	protected DecisionType action;
	protected int setLane;
	public Decision() {
		action = DecisionType.DoNothing;
	}
	public Decision(DecisionType type) {
		action = type;
		assert action != DecisionType.MakeRun : "MakeRun should use the constructor Decision(DecisionType, int)";
	}
	public Decision(DecisionType type, int L) {
		action = type;
		setLane = L;
		if (action == DecisionType.MakeRun) {
			flag[setLane] = 1;
			System.out.println("set lane " + setLane);
		}
	}
	public static boolean isEntry() {
		return (flag[0] & flag[1] & flag[2]) == 0; 
	}
	public DecisionType getAction() {
		return action;
	}
	public int getLane() {
		return setLane;
	}
}

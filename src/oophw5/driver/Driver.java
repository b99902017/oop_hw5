package oophw5.driver;

import oophw5.highway.*;

public abstract class Driver {
	public enum DriverState {
		WAIT, RUN, STOP, EXIT
	}
	protected DriverState state;
	public Driver() {
		state = DriverState.WAIT;
	}
	public void setState(DriverState ds) {
		state = ds;
	}
	public DriverState getState() {
		return state;
	}
	protected boolean isUpLane(Car self) {
		return self.getLane() == 0;
	}
	protected boolean isDownLane(Car self, Highway highway) {
		return self.getLane() == highway.getLane() - 1;
	}
	protected abstract Decision waitPolicy(Highway highway, Car car);
	protected abstract Decision runPolicy(Highway highway, Car car);
	protected abstract Decision stopPolicy(Highway highway, Car car);
	protected abstract Decision exitPolicy(Highway highway, Car car);
	public Decision behavior(Highway highway, Car car) {
		Decision res;
		switch (state) {
		case WAIT:
			res = waitPolicy(highway, car);
			break ;
		case RUN:
			res = runPolicy(highway, car);
			break ;
		case STOP:
			res = stopPolicy(highway, car);
			break ;
		case EXIT:
			res = exitPolicy(highway, car);
			break ;
		default:
			res = new Decision(Decision.DecisionType.DoNothing);
			break ;
		}
		if (res == null)
			res = new Decision(Decision.DecisionType.DoNothing);
		return res;
	}
}

package oophw5.simulator;

import java.awt.*;
import javax.swing.*;

import oophw5.*;
import oophw5.driver.*;
import oophw5.highway.*;

public final class Simulator extends JPanel implements Runnable {
	private static final long serialVersionUID = 1L;
	private Timer timer;
	private Highway highway;
	private int ncars;
	private Car[] car_list;

	public Simulator() {
		timer = new Timer();
		ncars = 0;
		highway = null;
		car_list = null;
		setLayout(new GridLayout(1, 1));
		setVisible(true);
	}
	public Simulator(int[] nDrivers, int length) {
		this();
		newHighway(length);
		newCarList(nDrivers);
		newTimer(10000); // 10 s
	}
	public Simulator(int[] nDrivers, int length, long deadline, long interval) {
		this();
		newHighway(length);
		newCarList(nDrivers);
		newTimer(deadline, interval);
	}
	public Simulator(int[] nDrivers, int length, long deadline) {
		this();
		newHighway(length);
		newCarList(nDrivers);
		newTimer(deadline);
	}
	private Driver getDriverType(int type) {
		switch (type) {
		case 0:
			return new GoodDriver();
		default:
			return null;
		}
	}
	public void newCarList(int[] nDrivers) {
		for (int i = 0; i < nDrivers.length; i++)
			ncars += nDrivers[i];
		car_list = new Car[ncars];
		int count = 0;
		for (int i = 0; i < nDrivers.length; i++) {
			for (int j = 0; j < nDrivers[i]; j++) {
				car_list[count] = new Car(String.format("car%03d", count), getDriverType(i), -1, highway);
				count++;
			}
		}
	}
	public Highway getHighway() {
		return highway;
	}
	public void newHighway(int length) {
		highway = new Formosa(length);
		
		setVisible(false);
		removeAll();
		add(highway.current);
		setVisible(true);
	}
	public void newTimer(long deadline, long interval) {
		timer = new Timer(0, deadline, interval);
	}
	public void newTimer(long deadline) {
		timer = new Timer(0, deadline);
	}
	public void run() {
		infoSimulator("Simulator runs.");
		for (int i = 0; i < ncars; i++)
			car_list[i].start();
		infoSimulator("All cars start.");
		for (Car car : car_list) {
			while (car.getState() != Thread.State.WAITING) ;
			infoSimulator(car.getCarName() + " is waiting.");
		}
		
		infoSimulator("Start simulate.");
		
		while (timer.oneTick()) {
			infoSimulator("Onk ticks.");
			Decision.flag = new int[]{0, 0, 0};
			highway.setNewWay();
			infoSimulator("Set up new way map.");
			synchronized (Key.key) {
				Key.key.notifyAll();
				infoSimulator("Wake up all cars.");
			}
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {}
			
			int check;
			for (check = 0; check < ncars; check++) {
				if (car_list[check].isAlive())
					break ;
			}
			if (check == ncars) {
				infoSimulator("All cars terminate.");
				return ;
			}
			for (Car car : car_list) {
				while (car.isAlive() && car.getState() != Thread.State.WAITING) ;
				infoSimulator(car.getCarName() + " is waiting.");
			}
			if (highway.isCollision() == true) {
				infoSimulator("Collision happened.");
				for (Car car : car_list)
					car.interrupt();
				return ;
			}
			Decision.flag = new int[]{0, 0, 0};
			highway.getNewWay();
			setVisible(false);
			removeAll();
			add(highway.current);
			setVisible(true);
		}
		
		for (Car car : car_list) {
			if (car.isAlive() == true)
				car.interrupt();
		}
	}
	
	private void infoSimulator(String str) {
		synchronized (Key.key) {
			System.out.println("[Simulator] " + str);
		}
	}
}

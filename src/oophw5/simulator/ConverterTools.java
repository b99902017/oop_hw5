package oophw5.simulator;

import oophw5.highway.*;

public final class ConverterTools {
	public static final double KMPStoMPS(double kmps) {
		return kmps * 1000 / 3600;
	}
	public static final double MyAbs(double num) {
		if (num < 0) return -num;
		return num;
	}
	public static final int MyMax(int x, int y) {
		if (x > y) return x;
		return y;
	}
	public static final int MyMin(int x, int y) {
		if (x < y) return x;
		return y;
	}
	public static final double distance(Car car1, Car car2) {
		return MyAbs(car1.getPosition() - car2.getPosition());
	}
	public static final double distance(Car car) {
		return car.getPosition();
	}
}

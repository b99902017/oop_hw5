package oophw5.simulator;

public final class Timer {
	private long time, deadline;
	private long msec;
	public Timer() {
		time = 0;	// 0 ms
		msec = 10;	// 0.01 second
	}
	public Timer(long t, long dl, long ms) {
		time = t * 1000;
		deadline = dl * 1000;
		msec = ms;
	}
	public Timer(long t, long dl) {
		this();
		time = t * 1000;
		deadline = dl * 1000;
	}
	public double getTime() {
		return time / 1000.0;
	}
	public void setTime(long t) {
		time = t * 1000;
	}
	public double getDeadline() {
		return deadline / 1000.0;
	}
	public void setDeadline(long dl) {
		deadline = dl * 1000;
	}
	public long getInterval() {
		return msec;
	}
	public void setInterval(long ms) {
		msec = ms;
	}
	public boolean oneTick() {
		time += msec;
		return time < deadline;
	}
}

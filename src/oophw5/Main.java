package oophw5;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;

import oophw5.simulator.*;

public class Main extends JApplet {
	private static final long serialVersionUID = 1L;
	public Thread SimThread;
	public Simulator sim;
	private JPanel info_shell, info_left, info_right;
	private JPanel highway_shell;
	private JTextField ngood_driver, length, deadline;
	private JButton start_btn;
	
	private class StartAction implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			takeAction();
		}
	}
	private void takeAction() {
		highway_shell.setVisible(false);
		highway_shell.removeAll();
		int gp = Integer.valueOf(ngood_driver.getText());
		int len = Integer.valueOf(length.getText());
		int dl = Integer.valueOf(deadline.getText());
		sim = new Simulator(new int[]{gp}, len, dl * 1000);
		highway_shell.add(sim);
		highway_shell.setVisible(true);
		
		
		SimThread = new Thread(sim);
		SimThread.start();
	}
	
	private void setInfo() {

		JPanel info;
		info = new JPanel();
		info.setLayout(new BorderLayout());
		info.setBorder(new TitledBorder("Arguments"));
		info.setSize(400, 300);
		
		int tmp = 4;
		info_left = new JPanel();
		info_right = new JPanel();
		info_left.setLayout(new GridLayout(tmp, 1));
		info_right.setLayout(new GridLayout(tmp, 1));
		
		ngood_driver = new JTextField("50", 12);
		info_left.add(new JLabel("num of good drivers: ", JLabel.RIGHT));
		info_right.add(ngood_driver);
		length = new JTextField("100", 12);
		info_left.add(new JLabel("highway length: ", JLabel.RIGHT));
		info_right.add(length);
		deadline = new JTextField("5", 12);
		info_left.add(new JLabel("simulation time(sec): ", JLabel.RIGHT));
		info_right.add(deadline);
		
		
		start_btn = new JButton("Start !!");
		start_btn.addActionListener(new StartAction());
		
		info.add(BorderLayout.WEST, info_left);
		info.add(BorderLayout.EAST, info_right);
		info.add(BorderLayout.SOUTH, start_btn);
		
		info_shell = new JPanel();
		info_shell.setSize(800, 300);
		info_shell.setLayout(new GridLayout(1, 3));
		info_shell.add(new JPanel());
		info_shell.add(info);
		info_shell.add(new JPanel());
		
		info.setVisible(true);
	}
	private void setHighwayView() {
		highway_shell = new JPanel();
		highway_shell.setLayout(new GridLayout(1, 1));
		highway_shell.add(sim);
		highway_shell.setBorder(new TitledBorder("Highway"));
	}
	public void init() {
		sim = new Simulator();
		setSize(1200, 600);
		setLayout(new GridLayout(4, 1));
		
		setInfo();
		setHighwayView();
		add(info_shell);
		add(new JPanel());
		add(highway_shell);
		add(new JPanel());
		
		setVisible(true);
	}
}

package oophw5.highway;

import java.awt.*;
import javax.swing.*;

import oophw5.driver.Driver;
import oophw5.driver.Driver.DriverState;
import oophw5.simulator.*;

public class Highway {
	public enum Flag {
		SAFE, COLLISION
	}
	protected Flag flag;
	protected String name;
	protected int lane, len;
	protected double speed_limit;
	protected Car[][] way, next_way;
	public HighwayView current, next;
	
	public Highway(String str, int width, int length, double limit) {
		flag = Flag.SAFE;
		name = str;
		lane = width;
		len = length;
		speed_limit = limit;
		way = new Car[lane][len];
		current = new HighwayView(way);
	}
	public boolean isCollision() {
		return flag == Flag.COLLISION;
	}
	public void setNewWay() {
		System.out.println("lane = " + lane + ", len = " + len);
		next_way = new Car[lane][len];
	}
	public void getNewWay() {
		way = next_way;
		current = new HighwayView(way);
	}
	public double getSpeedLimit() {
		return speed_limit;
	}
	public int getLength() {
		return len;
	}
	public int getLane() {
		return lane;
	}
	public void updateCar(Car car) {
		DriverState st = car.getDriver().getState();
		if (st != Driver.DriverState.WAIT && st != Driver.DriverState.EXIT) {
			if (next_way[car.lane][car.getPosition()] == null) {
				next_way[car.lane][car.getPosition()] = car;
			}
			else flag = Flag.COLLISION;
		}
		int tmp = 0;
		for (int i = ConverterTools.MyMin(len - 1, car.getPosition() - 1); i >= 0; i--) {
			if (next_way[car.lane][i] == car) {
				if (tmp > 0) flag = Flag.COLLISION;
				return ;
			}
			if (next_way[car.lane][i] != null) tmp++;
		}
	}
	
	private class HighwayView extends JPanel {
		private static final long serialVersionUID = 1L;
		public HighwayView(Car[][] car_list) {
			setLayout(new GridLayout(lane * 2 + 1, len));
			for (int i = 0; i < lane * 2 + 1; i++) {
				for (int j = 0; j < len; j++) {
					if (i % 2 == 0) {
						JPanel tmp = new JPanel();
						tmp.setBackground(Color.black);
						this.add(tmp);
					}
					else if (car_list[i / 2][j] == null) {
						JPanel tmp = new JPanel();
						tmp.setBackground(Color.black);
						this.add(tmp);
					}
					else {
						JPanel tmp = new JPanel();
						tmp.setBackground(Color.yellow);
						this.add(tmp);
					}
				}
			}
			setVisible(true);
		}
	}
	public HighwayView getHighwayView() {
		return current;
	}
}

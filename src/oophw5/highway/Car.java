package oophw5.highway;

import oophw5.driver.*;
import oophw5.*;

public final class Car extends Thread {
	private String name;
	protected Driver driver;
	protected int lane;
	private double pos;
	protected double speed;
	protected Highway highway;
	public Car(String str, Driver d, int p, Highway hw) {
		name = str;
		driver = d;
		pos = (double)p;
		highway = hw;
		
		lane = 1;
		speed = 0;
	}
	public String getCarName() {
		return name;
	}
	public Driver getDriver() {
		return driver;
	}
	public void setDriver(Driver d) {
		driver = d;
	}
	public int getLane() {
		return lane;
	}
	public void setLane(int L) {
		lane = L;
	}
	public int getPosition() {
		return (int)pos;
	}
	public void setPosition(int p) {
		pos = (double)p;
	}
	public void setPosition(double p) {
		pos = p;
	}
	public double getSpeed() {
		return speed;
	}
	public void setSpeed(double sp) {
		speed = sp;
	}
	public boolean reaction() {
		Decision decision = driver.behavior(highway, this);
		switch (decision.getAction()) {
		case DoNothing:
			infoCar("do nothing.");
			break;
		case MakeWait:
			speed = 0;
			driver.setState(Driver.DriverState.WAIT);
			infoCar("make wait.");
			break;
		case MakeRun:
			lane = decision.getLane();
			pos = 0;
			speed = 0.1;
			driver.setState(Driver.DriverState.RUN);
			infoCar("make run.");
			break;
		case MakeStop:
			if (speed > 0.1)
				speed -= 0.2;
			else {
				speed = 0;
				driver.setState(Driver.DriverState.STOP);
			}
			infoCar("make stop.");
			break;
		case MakeExit:
			speed = 0;
			driver.setState(Driver.DriverState.EXIT);
			infoCar("make exit.");
			break;
		case GoUpside:
			lane -= 1;
			infoCar("go upside.");
			break;
		case GoDownside:
			lane += 1;
			infoCar("go downside.");
			break;
		case SpeedUp:
			speed += 0.1;
			infoCar("speed up.");
			break;
		case SpeedDown:
			if (speed > 0.1)
				speed -= 0.1;
			else {
				speed = 0;
				driver.setState(Driver.DriverState.STOP);
			}
			infoCar("speed down.");
			break;
		default:
			return false;
		}
		synchronized (Key.key) {
			highway.updateCar(this);
		}
		return true;
	}
	public void run() {
		infoCar("starts.");
		try {
			infoCar("wait for wake up.");
			synchronized (Key.key) {
				Key.key.wait();
			}
			while (driver.getState() != Driver.DriverState.EXIT) {
				synchronized (Key.key) {
					infoCar("start take action.");
					pos += speed;
					if (getPosition() >= highway.len) {
						driver.setState(Driver.DriverState.EXIT);
					}
					reaction();
					Key.key.wait();
				}
			}
		} catch (InterruptedException e) {}
		infoCar("is terminating.");
	}
	
	private void infoCar(String str) {
		synchronized (Key.key) {
			System.out.println("[Car] " + name + " " + str + " Lane " + lane + ", pos " + getPosition() + ".");
		}
	}
}

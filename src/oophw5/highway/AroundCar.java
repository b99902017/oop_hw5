package oophw5.highway;

// import oophw5.*;

public final class AroundCar {
	private Car[] cars;
	public AroundCar(Car[] cs) {
		cars = cs;
	}
	public AroundCar() {
		this(new Car[7]);
	}
	public AroundCar(Highway highway, Car car) {
		this(new Car[7]);
		int[] dlane = new int[]{0, 0, -1, -1, 1, 1};
		int[] dpos = new int[]{1, -1, 1, -1, 1, -1};
		assert dlane.length == dpos.length : "��l�ƿ��~";
		
		cars[6] = car;
		
		int length = dlane.length;
		for (int sit = 0; sit < length; sit++) {
			cars[sit] = getAround(highway, car.lane + dlane[sit], car.getPosition(), dpos[sit]);
		}
	}
	
	private Car getAround(Highway highway, int clane, int cpos, int step) {
		if (clane < 0 || highway.lane <= clane)
			return null;
		for (int i = cpos + step; 0 <= i && i < highway.len; i += step)
			if (highway.way[clane][i] != null)
				return highway.way[clane][i] ;
		return null;
	}
	
	public Car front() {
		return cars[0];
	}
	public Car back() {
		return cars[1];
	}
	public Car upfront() {
		return cars[2];
	}
	public Car upback() {
		return cars[3];
	}
	public Car downfront() {
		return cars[4];
	}
	public Car downback() {
		return cars[5];
	}
	public Car self() {
		return cars[6];
	}
	
	/*
	private void infoAroundCar(String str) {
		synchronized (Key.key) {
			System.out.println("[Around Car] " + str);
		}
	}
	*/
}

RM=rm -rf
JC=javac
SRCPATH=src
CLSPATH=bin
JFLAGS=-sourcepath $(SRCPATH) -classpath $(CLSPATH) -d $(CLSPATH) -g
PROG= \
	oophw5/Main \
	oophw5/Key \
	oophw5/driver/Driver \
	oophw5/driver/GoodDriver \
	oophw5/driver/Decision \
	oophw5/highway/Car \
	oophw5/highway/AroundCar \
	oophw5/highway/Highway \
	oophw5/highway/Formosa \
	oophw5/simulator/ConverterTools \
	oophw5/simulator/Simulator \
	oophw5/simulator/Timer
SRC=$(patsubst %, src/%.java, $(PROG))

JAR=jar
JARFLAGS=-cvf

.SUFFIXES:
.SUFFIXES: .java .class

all:
	mkdir $(CLSPATH)
	$(JC) $(JFLAGS) $(SRC)
	$(JAR) $(JARFLAGS) hw5.jar -C $(CLSPATH) .

clean:
	$(RM) $(CLSPATH)
